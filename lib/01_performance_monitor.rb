def measure(count=1, &block)
  total_duration = 0
  count.times do
    start_time = Time.now
    block.call
    end_time = Time.now
    total_duration += end_time - start_time
  end
  total_duration / count
end

# def measure(pass = 1)
#     start_time = Time.now
#     pass.times {yield}
#     (Time.now - start_time) / (pass)
# end
