# ### Factors
#
# Write a method `factors(num)` that returns an array containing all the
# factors of a given number.

def factors(num)
  (1..num).to_a.select { |number| num % number == 0}
end

# ### Bubble Sort
#
# http://en.wikipedia.org/wiki/bubble_sort
#
# Implement Bubble sort in a method, `Array#bubble_sort!`. Your method should
# modify the array so that it is in sorted order.
#
# > Bubble sort, sometimes incorrectly referred to as sinking sort, is a
# > simple sorting algorithm that works by repeatedly stepping through
# > the list to be sorted, comparing each pair of adjacent items and
# > swapping them if they are in the wrong order. The pass through the
# > list is repeated until no swaps are needed, which indicates that the
# > list is sorted. The algorithm gets its name from the way smaller
# > elements "bubble" to the top of the list. Because it only uses
# > comparisons to operate on elements, it is a comparison
# > sort. Although the algorithm is simple, most other algorithms are
# > more efficient for sorting large lists.
#
# Hint: Ruby has parallel assignment for easily swapping values:
# http://rubyquicktips.com/post/384502538/easily-swap-two-variables-values
#
# After writing `bubble_sort!`, write a `bubble_sort` that does the same
# but doesn't modify the original. Do this in two lines using `dup`.
#
# Finally, modify your `Array#bubble_sort!` method so that, instead of
# using `>` and `<` to compare elements, it takes a block to perform the
# comparison:
#
# ```ruby
# [1, 3, 5].bubble_sort! { |num1, num2| num1 <=> num2 } #sort ascending
# [1, 3, 5].bubble_sort! { |num1, num2| num2 <=> num1 } #sort descending
# ```
#
# #### `#<=>` (the **spaceship** method) compares objects. `x.<=>(y)` returns
# `-1` if `x` is less than `y`. If `x` and `y` are equal, it returns `0`. If
# greater, `1`. For future reference, you can define `<=>` on your own classes.
#
# http://stackoverflow.com/questions/827649/what-is-the-ruby-spaceship-operator

class Array
  def bubble_sort!(&prc)
    p prc
    prc = Proc.new { |el1, el2| el1 <=> el2} if prc.nil?

    sorted = false #assume the array isn't sorted
    until sorted == true #keep looping until the array is sorted
      sorted = true #use as default with false assignment used in each loop conditional

      self.each_with_index do | el, idx |
        if idx < length - 1 && prc.call(el, self[idx + 1]) == 1
          # 1st part makes sure self[idx + 1] won't be nil, while 2nd part determines sort based on Proc

          #### IMPORTANT TO UNDERSTAND THIS CONDITIONAL, the proc either given or defaulted to should always be equal to 1 regardless of ascending or descending order

          #ascending ex: { |el1, el2| el1 <=> el2}, if left side is larger, it will ALWAYS EQUAL 1. When el1 is larger than el2, since you want the smaller number (el2) in front for ascending order, you need to reassign the variables so that el1 is moves to the end of the array AS THE GREATER VALUE.

          #descending ex: { |el1, el2| el2 <=> el1}, operator stays the same, if the left side is larger, it will still equal 1. Now you want el1 to be smaller, but due to spaceship operator being static in how it returns its result (1/0/-1, >/==/<) and you want the larger number (el2) in the front for descending order, the proc switches the positions to (el2, el1) so that if el2 is bigger than el1, el1 moves to the end of the array AS THE LESSER VALUE.
          self[idx], self[idx + 1] = self[idx + 1], self[idx]
          #self must be used in order to modify the array, as el will merely change the temp object rather than the actual object in the array
          sorted = false
        end
      end

    end
    self
  end

  def bubble_sort(&prc)
    p self
    self.dup.bubble_sort! &prc
  end
end

# ### Substrings and Subwords
#
# Write a method, `substrings`, that will take a `String` and return an
# array containing each of its substrings. Don't repeat substrings.
# Example output: `substrings("cat") => ["c", "ca", "cat", "a", "at",
# "t"]`.
#
# Your `substrings` method returns many strings that are not true English
# words. Let's write a new method, `subwords`, which will call
# `substrings`, filtering it to return only valid words. To do this,
# `subwords` will accept both a string and a dictionary (an array of
# words).

def substrings(string)
  substring_array = []

  string.split("").each_with_index do |letter, idx|
    idx2 = idx

    while idx2 < string.length
      substring_array << string[idx..idx2]
      idx2 += 1
    end

  end
  substring_array
end

def subwords(word, dictionary)
  substrings(word).select { |substring| dictionary.include?(substring) }.uniq
end

# ### Doubler
# Write a `doubler` method that takes an array of integers and returns an
# array with the original elements multiplied by two.

def doubler(array)
  array.map { |num| num * 2 }
end

# ### My Each
# Extend the Array class to include a method named `my_each` that takes a
# block, calls the block on every element of the array, and then returns
# the original array. Do not use Enumerable's `each` method. I want to be
# able to write:
#
# ```ruby
# # calls my_each twice on the array, printing all the numbers twice.
# return_value = [1, 2, 3].my_each do |num|
#   puts num
# end.my_each do |num|
#   puts num
# end
# # => 1
#      2
#      3
#      1
#      2
#      3
#
# p return_value # => [1, 2, 3]
# ```

class Array
  def my_each(&prc)
    idx = 0

    while idx < self.length
      prc.call(self[idx])
      idx += 1
    end
    self
  end
end

# ### My Enumerable Methods
# * Implement new `Array` methods `my_map` and `my_select`. Do
#   it by monkey-patching the `Array` class. Don't use any of the
#   original versions when writing these. Use your `my_each` method to
#   define the others. Remember that `each`/`map`/`select` do not modify
#   the original array.
# * Implement a `my_inject` method. Your version shouldn't take an
#   optional starting argument; just use the first element. Ruby's
#   `inject` is fancy (you can write `[1, 2, 3].inject(:+)` to shorten
#   up `[1, 2, 3].inject { |sum, num| sum + num }`), but do the block
#   (and not the symbol) version. Again, use your `my_each` to define
#   `my_inject`. Again, do not modify the original array.

class Array
  def my_map(&prc)
    map = []

    self.my_each do |ele|
      map << prc.call(ele)
    end

    map
  end

  def my_select(&prc)
    select_array = []

    self.my_each do |ele|
      select_array << ele if prc.call(ele)
    end

    select_array
  end

  def my_inject(&blk)
    total = self[0]

    self[1..-1].my_each do |ele|
      total = blk.call(total, ele)
    end

    total
  end
end

# ### Concatenate
# Create a method that takes in an `Array` of `String`s and uses `inject`
# to return the concatenation of the strings.
#
# ```ruby
# concatenate(["Yay ", "for ", "strings!"])
# # => "Yay for strings!"
# ```

def concatenate(strings)
  strings.inject("") { |concatenated, string| concatenated += string}
end
