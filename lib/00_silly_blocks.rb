# Go over this again
def reverser
  word_arr = yield.split(" ")

  word_arr.each { |word| word.reverse!}
  word_arr.join(" ")

end

def adder(add = 1)
  yield + add
end

def repeater(parameter = 0)
  return yield if parameter == 0
  parameter.times { yield }
end
